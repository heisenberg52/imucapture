package com.simplesolutionscompany.imucapture;


import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by f.fong on 3/10/2016.
 */
final public class SensorDataChangedOnSubscribe implements Observable.OnSubscribe<float[]>{

    Sensor mSensor;
    SensorManager mSensorManager;

    public SensorDataChangedOnSubscribe(Sensor sensor, SensorManager sensorManager) {
        mSensor = sensor;
        mSensorManager = sensorManager;
    }

    @Override
    public void call(final Subscriber<? super float[]> subscriber) {

        mSensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(sensorEvent.values);
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        }, mSensor, SensorManager.SENSOR_DELAY_NORMAL);

    }
}
