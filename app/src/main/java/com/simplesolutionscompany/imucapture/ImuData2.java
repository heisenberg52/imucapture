package com.simplesolutionscompany.imucapture;

/**
 * Created by f.fong on 3/11/2016.
 */
public class ImuData2 {

    public float R[]; // rotational matrix

    public ImuData2 (float[] rotateM) {

        if (rotateM.length== 16) {
            R = new float[16];
            System.arraycopy(rotateM, 0, R, 0, rotateM.length);
        }
        else {
            throw new IllegalArgumentException("must be 4x4 matrix (16-element array)");
        }
    }

    @Override public String toString() {
        return String.format("R0: %.2f %.2f %.2f %.2f\n" + "R1: %.2f %.2f %.2f %.2f \n" +
                        "R2: %.2f %.2f %.2f %.2f\n" + "R3: %.2f %.2f %.2f %.2f",
                R[0], R[1], R[2], R[3], R[4], R[5], R[6], R[7],
                R[8], R[9], R[10], R[11], R[12], R[13], R[14], R[15]);
    }
}
