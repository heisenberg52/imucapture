package com.simplesolutionscompany.imucapture;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;

import rx.Observable;

/**
 * Created by f.fong on 3/10/2016.
 */
public class RxSensor {

    public static Observable<float[]> dataChanges(@NonNull Sensor sensor, @NonNull SensorManager sensorManager) {
        assert (sensor != null);
        return Observable.create(new SensorDataChangedOnSubscribe(sensor, sensorManager));
    }

}
