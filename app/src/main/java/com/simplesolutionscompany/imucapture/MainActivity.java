package com.simplesolutionscompany.imucapture;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.jakewharton.rxbinding.widget.RxCompoundButton;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements Handler.Callback {

    private static final String TAG = "MainActivity";

    private static final int MSG_TYPE_TOAST = 1;
    private static final String MSG_KEY_STRMESSAGE = "TOASTMSG";

    private ImuSender imuSender;
    private Handler mIncomingHandler;



    @Override
    public boolean handleMessage(Message message) {

        switch (message.what) {
            case MSG_TYPE_TOAST:
                String tMsg = message.getData().getString(MSG_KEY_STRMESSAGE);
                Toast t = Toast.makeText(getApplicationContext(), tMsg, Toast.LENGTH_LONG);
                t.show();
                break;
        }
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mIncomingHandler = new Handler(this);

        CompoundButton connectButton = (CompoundButton) findViewById(R.id.connectButton);
        RxCompoundButton.checkedChanges(connectButton).observeOn(Schedulers.io())
                .subscribe(
                        // onNext
                        new Action1<Boolean>() {
                            @Override
                            public void call(Boolean isChecked) {
                                if (isChecked) {
                                    if (imuSender == null) {
                                        imuSender = new ImuSender();
                                    }
                                    EditText textInput = (EditText) findViewById(R.id.hostEditText);
                                    String remoteIP = textInput.getText().toString();
                                    textInput = (EditText) findViewById(R.id.portEditText);
                                    String portStr = textInput.getText().toString();
                                    String toastMessage;
                                    if (remoteIP != null && portStr != null
                                            && remoteIP.length() > 0 && portStr.length() > 0) {
                                        try {
                                            int portNum = Integer.valueOf(portStr);
                                            boolean succ = false;
                                            synchronized (imuSender) {
                                                succ = imuSender.setDestination(remoteIP, portNum);
                                            }
                                            if (succ) {
                                                toastMessage = "Remote Destination Set";
                                            } else {
                                                toastMessage = "Remote Destination not set";
                                            }
                                        } catch (NumberFormatException nfe) {
                                            toastMessage = "Port must be a number";
                                        }
                                    } else {
                                        toastMessage = "Fields cannot be empty";
                                    }
                                    Message m = Message.obtain(mIncomingHandler, MSG_TYPE_TOAST);
                                    Bundle b = new Bundle();
                                    b.putString(MSG_KEY_STRMESSAGE, toastMessage);
                                    m.setData(b);
                                    m.sendToTarget();
                                } else {
                                    if (imuSender != null) {
                                        imuSender.close();
                                        imuSender = null;
                                    }
                                }

                            }
                        },
                        // onError
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                                String toastMessage = throwable.getMessage();
                                if (toastMessage == null || toastMessage.length() == 0) {
                                    toastMessage = "Error setting datagram destination";
                                }
                                Message m = Message.obtain(mIncomingHandler, MSG_TYPE_TOAST);
                                Bundle b = new Bundle();
                                b.putString(MSG_KEY_STRMESSAGE, toastMessage);
                                m.setData(b);
                                m.sendToTarget();
                            }
                        }
                );

        // Accelerator Observer
        SensorManager sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        //Sensor accelSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER); // includes Gravity
        Sensor accelSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION); // excludes Gravity

        if (accelSensor == null) {
            Log.w(TAG, "Accelerator sensor not available!");
            return;
        }

        Observable<float[]> accelObservable = RxSensor.dataChanges(accelSensor, sensorManager);


        // Gyroscope Observer
        Sensor gyroSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        if (gyroSensor == null) {
            Log.w(TAG, "Gyro sensor not available!");
            return;
        }

        Observable<float[]> gyroObservable = RxSensor.dataChanges(gyroSensor, sensorManager);


        // Merge (CombineLatest)
        Observable<ImuData> combinedLatest = Observable.combineLatest(accelObservable, gyroObservable, new Func2<float[], float[], ImuData>() {
            @Override public ImuData call (float[] accelData, float[] gyroData) {
                // combine accel (X,Y,Z) with gyroData (X,Y,Z)
                ImuData combinedData = null;
                if (accelData.length > 0 && gyroData.length > 0) {
                    //float[] R = new float[16]; // rotational matrix
                    //SensorManager.getRotationMatrixFromVector(R, gyroData);
                    combinedData = new ImuData(accelData, gyroData);
                    Log.d(TAG, combinedData.toString());
                }
                return combinedData;
            }
        });

        combinedLatest.subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(
                new Action1<ImuData>() {
                     @Override
                     public void call(ImuData imuData) {
                         if (imuSender != null && imuData != null) {
                             synchronized (imuSender) {
                                 imuSender.sendData(imuData);
                             }
                         }
                     }
                 }, new Action1<Throwable>() {
                     @Override
                     public void call(Throwable throwable) {
                         String toastMessage = throwable.getMessage();
                         if (toastMessage == null || toastMessage.length() == 0) {
                             toastMessage = "Error sending IMU data";
                         }
                         Message m = Message.obtain(mIncomingHandler, MSG_TYPE_TOAST);
                         Bundle b = new Bundle();
                         b.putString(MSG_KEY_STRMESSAGE, toastMessage);
                         m.setData(b);
                         m.sendToTarget();
                     }
                 }


        );


    }



}
