package com.simplesolutionscompany.imucapture;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Calendar;

/**
 * Created by f.fong on 3/8/2016.
 */
public class ImuSender {

    private DatagramSocket socket;
    private static final String TAG = "ImuSender";

    // use default constructor

    public boolean setDestination (String host, int port) {
        if (socket == null) {
            try {
                socket = new DatagramSocket();
            }
            catch (SocketException se) {
                Log.e(TAG, "Error creating socket: " + se.getMessage());
                se.printStackTrace();
                return false;
            }
        }
        if (socket.isConnected()) {
            socket.disconnect();
        }
        InetSocketAddress remoteHost = new InetSocketAddress(host, port);
        try {
            socket.connect(remoteHost);
            return true;
        }
        catch (java.net.SocketException se) {
            Log.e(TAG, "Error connecting: " + se.getMessage());
            se.printStackTrace();
        }
        return false;
    }

    public boolean sendData (ImuData data) {
        if (socket != null && socket.isConnected()) {

            // convert data into string
            // time, accel-x, accel-y, accel-z, angle-x, angle-y, angle-z
            String dataString = String.format("%d,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f",
                    Calendar.getInstance().getTimeInMillis(),
                    data.accelX, data.accelY, data.accelZ,
                    data.angleX, data.angleY, data.angleZ);

            byte dataBytes[] = dataString.getBytes();
            if (dataBytes.length > 0) {
                DatagramPacket dp = new DatagramPacket(dataBytes, dataBytes.length);
                try {
                    socket.send(dp);
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "Error Sending Packet: " + e.getMessage());
                }
            }
        }

        return false;
    }

    public boolean sendData (ImuData2 data) {
        if (socket != null && socket.isConnected()) {

            // convert data into string
            // time, accel-x, accel-y, accel-z, angle-x, angle-y, angle-z
            String dataString = String.format("%d,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f",
                    Calendar.getInstance().getTimeInMillis(),
                    data.R[0], data.R[1], data.R[2], data.R[3],
                    data.R[4], data.R[5], data.R[6], data.R[7],
                    data.R[8], data.R[9], data.R[10], data.R[11],
                    data.R[12], data.R[13], data.R[14], data.R[15]);


            byte dataBytes[] = dataString.getBytes();
            if (dataBytes.length > 0) {
                DatagramPacket dp = new DatagramPacket(dataBytes, dataBytes.length);
                try {
                    socket.send(dp);
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "Error Sending Packet: " + e.getMessage());
                }
            }
        }

        return false;
    }
    public void close() {
        if (socket != null ) {
            if (socket.isConnected())
                socket.disconnect();
            if (!socket.isClosed()) {
                socket.close();
            }
            socket = null;
        }
    }
}
