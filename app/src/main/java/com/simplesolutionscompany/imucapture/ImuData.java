package com.simplesolutionscompany.imucapture;

/**
 * Created by f.fong on 3/8/2016.
 */
public class ImuData {

    public float accelX;
    public float accelY;
    public float accelZ;
    public float angleX;
    public float angleY;
    public float angleZ;

    public ImuData (float[] accel, float[] angles) {

            if (accel.length >= 3 && angles.length >= 3) {
                accelX = accel[0];
                accelY = accel[1];
                accelZ = accel[2];
                angleX = angles[0];
                angleY = angles[1];
                angleZ = angles[2];
            }
            else {
                throw new IllegalArgumentException("array size must be at least 3");
            }

    }

    @Override public String toString() {
        return String.format("AX: %.2f AY: %.2f AZ: %.2f    GX: %.2f GY: %.2f GZ: %.2f",
                accelX, accelY, accelZ, angleX, angleY, angleZ);
    }
}
